/*
    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://www.wtfpl.net/ for more details.
*/

import FormAssociatedElement from './form-associated.js';

class InputCurrency extends FormAssociatedElement {

    _currencySymbol;
    _groupingSymbol;
    _decimalSymbol;

    _wrapperElement;
    _currencyElement;
    _inputElement;

    _disabled = false;

    constructor() {
        super();

        this._currencySymbol = this.getAttribute('currency') ?? '&#129689;';

        this._groupingSymbol = this.getAttribute('groupingsymbol') ?? '';

        this._decimalSymbol = this.getAttribute('decimalsymbol') ?? '.';

        let newValue = parseInt(this.getAttribute('value'));
        if (newValue > Number.MAX_SAFE_INTEGER)
            newValue = Number.MAX_SAFE_INTEGER;
        if (newValue < 0)
            newValue = 0;
        this._value = isNaN(newValue) ? 0 : newValue;
        this._internals.setFormValue(this._value);

        const shadow = this.attachShadow({ mode: 'open' });

        this._wrapperElement = document.createElement('div');
        this._wrapperElement.id = 'wrapper';
        this._wrapperElement.part = 'element';

        this._currencyElement = document.createElement('span');
        this._currencyElement.id = 'currency';
        this._currencyElement.part = 'inner currency';

        this._inputElement = document.createElement('span');
        this._inputElement.id = 'value';
        this._inputElement.part = 'inner input';

        this.tabIndex = 0;
        this.addEventListener('keydown', this._handleInput);

        const style = document.createElement('style');
        style.textContent = //css
        `
            :host {
                display: inline-block;
                font-size: 1em;
                line-height: 1em;
                width: 10em;
            }

            .disabled {
                background-color: lightgray;
            }

            span {
                display: inline-block;
                padding: 0.2rem 0.3rem 0.2rem 0.3rem;
                vertical-align: middle;
            }

            #wrapper {
                display: inline-flex;
                width: 100%;
                border: 1px solid gray;
                border-radius: 2px;
            }

            #currency {
                text-align: center;
                width: 1em;
                border-right: 1px solid gray;
            }

            #value {
                direction: rtl;
                overflow-x: hidden;
                flex-grow: 1;
            }
        `;

        this._updateVisual();

        shadow.appendChild(style);
        shadow.appendChild(this._wrapperElement);
        this._wrapperElement.appendChild(this._currencyElement);
        this._wrapperElement.appendChild(this._inputElement);
    }

    /**
     * @param {string} v
     */
    __set_value(v) {
        const oldValue = this._value;
        let newValue = parseInt(v);
        if (isNaN(newValue))
            throw new Error('Value must be an integer');
        if (newValue > Number.MAX_SAFE_INTEGER)
            newValue = Number.MAX_SAFE_INTEGER;
        if (newValue < 0)
            newValue = 0;
        this._value = newValue;
        this._internals.setFormValue(newValue);
        if (oldValue != newValue)
            this._updateVisual();
    }

    get currencySymbol() { return this._currencySymbol; }
    /**
     * @param {string} v
     */
    set currencySymbol(v) {
        const oldValue = this._currencySymbol;
        this._currencySymbol = v;
        if (oldValue != v)
            this._updateVisual();
    }

    get groupingSymbol() { return this._groupingSymbol; }
    /**
     * @param {string} v
     */
    set groupingSymbol(v) {
        const oldValue = this._groupingSymbol;
        this._groupingSymbol = v;
        if (oldValue != v)
            this._updateVisual();
    }

    get decimalSymbol() { return this._decimalSymbol; }
    /**
     * @param {string} v
     */
    set decimalSymbol(v) {
        const oldValue = this._decimalSymbol;
        this._decimalSymbol = v;
        if (oldValue != v)
            this._updateVisual();
    }

    /**
     * @param {KeyboardEvent} event
     */
    _handleInput = (event) => {
        if (event.key === 'Tab')
            return;

        event.preventDefault();
        if (this._disabled) return;

        if (/^\d$/.test(event.key)) {
            this.value = this._value * 10 + parseInt(event.key);
        }

        if (event.key == 'Backspace') {
            if (this._value <= 9)
                this.value = 0;
            else
                this.value = Math.round((this._value - this._value % 10) / 10);
        }

        if (event.key == 'Delete') {
            this.value = 0;
        }
    }

    formDisabledCallback(disabled) {
        this._disabled = disabled;
        this._inputElement.contentEditable = !disabled;

        if (disabled) {
            this._wrapperElement.classList.add('disabled');
        } else {
            this._wrapperElement.classList.remove('disabled');
        }
    }

    /**
     * @returns {string}
     */
    getStringRepresentation() {
        const decimal = (this._value % 100).toString().padStart(2, '0');
        const real = Math.round((this._value - decimal) / 100).toString();

        let output = real + this._decimalSymbol + decimal;
        let pos = real.length;
        while (pos > 3) {
            pos -= 3;
            output = output.substring(0, pos) + this._groupingSymbol + output.substring(pos, output.length);
        }

        return output;
    }

    _updateVisual() {
        this._currencyElement.innerHTML = this._currencySymbol;
        this._inputElement.textContent = this.getStringRepresentation();
    }

}

customElements.define('input-currency', InputCurrency)
