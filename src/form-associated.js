/*
    This program is free software. It comes without any warranty, to
    the extent permitted by applicable law. You can redistribute it
    and/or modify it under the terms of the Do What The Fuck You Want
    To Public License, Version 2, as published by Sam Hocevar. See
    http://www.wtfpl.net/ for more details.
*/

class FormAssociatedElement extends HTMLElement {

    static formAssociated = true;

    constructor() {
        super();
        this._internals = this.attachInternals();
        this._value = 0;
    }

    get value() { return this.__get_value(); }
    set value(v) { this.__set_value(v); }

    get form() { return this._internals.form; }
    get name() { return this.getAttribute('name'); }
    get type() { return this.localName; }
    get validity() { return this._internals.validity; }
    get validationMessage() { return this._internals.validationMessage; }
    get willValidate() { return this._internals.willValidate; }

    __get_value() { return this._value; }
    __set_value(v) { this._value = v; }

    checkValidity() { return this._internals.checkValidity(); }
    reportValidity() { return this._internals.reportValidity(); }

}

export default FormAssociatedElement;
